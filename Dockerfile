# Use an official Python runtime as a parent image
FROM python:3.6-slim

EXPOSE 80

ARG POETRY_VERSION="1.0.0"

# System deps:
RUN apt-get clean \
    && apt-get -y update
RUN apt-get -y install nginx \
    && apt-get -y install python3-dev \
    && apt-get -y install build-essential
RUN pip install "poetry==$POETRY_VERSION"

# Set the working directory to /code
WORKDIR /code

# Copy across all the Docker tools
COPY docker_tools/nginx.conf /etc/nginx
COPY docker_tools/uwsgi.ini /code/uwsgi.ini
COPY docker_tools/startup.sh /code/startup.sh
RUN chmod +x /code/startup.sh

# Install app dependencies
COPY poetry.lock pyproject.toml /code/
RUN poetry config virtualenvs.create false \
  && poetry install --no-dev --no-interaction --no-ansi --extras "docker"

# Add the application to the docker image
COPY logging.yaml /code/logging.yaml
COPY internet_monitor /code/internet_monitor
COPY run.py /code/run.py

# Create the volume to persist the test results
VOLUME /app/data/

ENTRYPOINT ["./startup.sh"]
