from datetime import datetime
from unittest.mock import MagicMock, call

from monitor_common.mongodb.status_entry import StatusEntry

from internet_monitor.monitor.notifiers.entry_writer_notifier import (
    EntryWriterNotifier,
    EntryWriter,
)
from internet_monitor.monitor.status_tracker import Status, StatusChange


class TestEntryWriterNotifier:
    def test_entry_writer_notify(self):
        entry_writer = EntryWriter()
        entry_writer.write_new_entry = MagicMock()

        entry_writer_notifier = EntryWriterNotifier(entry_writer)

        entry_writer_notifier.notify(
            time=datetime(2018, 8, 3),
            result=True,
            status=Status.OK,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        assert entry_writer.write_new_entry.call_args_list
        assert entry_writer.write_new_entry.call_args_list == [
            call(
                StatusEntry(
                    time=datetime(2018, 8, 3),
                    result=True,
                    status=Status.OK,
                    status_change=StatusChange.ERROR_RESOLVED,
                )
            )
        ]
