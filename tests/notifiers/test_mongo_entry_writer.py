from datetime import datetime
from monitor_common.status import Status, StatusChange
from monitor_common.mongodb.status_entry import StatusEntry

from internet_monitor.monitor.notifiers.mongo_entry_writer import MongoEntryWriter


class TestMongoEntryWriter:
    def test_write_new_entry(self, mocked_db_clean):
        mongo_writer = MongoEntryWriter()

        entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        mongo_writer.write_new_entry(entry)

        entries = StatusEntry.objects
        saved_entry = entries.first()

        assert entries.count() == 1
        assert saved_entry == entry
