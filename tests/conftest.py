import docker
import os
import pytest
import mongoengine


class MockedDB:
    def __init__(self, in_gitlab_ci):
        if in_gitlab_ci:
            self._db = mongoengine.connect("test", alias="monitor", host="mongo")
        else:
            self._db = mongoengine.connect("test", alias="monitor")

    def clean_db(self):
        self._db.drop_database("test")


@pytest.fixture(scope="session")
def mocked_db():
    try:
        os.environ["CI"]
    except KeyError:
        in_gitlab_ci = False
    else:
        in_gitlab_ci = True

    if not in_gitlab_ci:
        client = docker.from_env()
        container = client.containers.run(
            "mongo", detach=True, ports={"27017/tcp": 27017}, remove=True
        )

    yield MockedDB(in_gitlab_ci)

    if not in_gitlab_ci:
        container.stop()


@pytest.fixture(scope="function")
def mocked_db_clean(mocked_db):
    mocked_db.clean_db()
    yield mocked_db
    mocked_db.clean_db()
