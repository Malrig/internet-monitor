from internet_monitor.monitor.connection_tester import ConnectionTester


class TestConnectionTester:
    def test_current_real_connection(self):
        connection_tester = ConnectionTester("www.google.com")
        have_connection = connection_tester.run_test()

        assert (
            have_connection
        ), "No connection established, this could be due to your connection being down or a software bug."
