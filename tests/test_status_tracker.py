import pytest
from datetime import datetime
from freezegun import freeze_time
from internet_monitor.monitor.status_tracker import StatusChange, Status, StatusTracker


class TestStatusTracker:
    def _submit_result_and_expect(
        self, status_tracker, to_submit, expect_status, expect_status_change
    ):
        status_change = status_tracker.submit_result(to_submit)

        assert expect_status == status_tracker.status
        assert expect_status_change == status_change

    def test_initialised_correctly(self):
        status_tracker = StatusTracker()

        # External properties
        assert Status.UNKNOWN == status_tracker.status
        assert datetime.min == status_tracker.last_success
        # Internal properties
        assert 5 == status_tracker._number_retry_attempts
        assert 0 == status_tracker._number_failures

    @freeze_time("2018-08-03")
    def test_submit_result_status_first_ok(self):
        status_tracker = StatusTracker()

        self._submit_result_and_expect(
            status_tracker, True, Status.OK, StatusChange.NONE
        )

        assert datetime(2018, 8, 3) == status_tracker.last_success

    @freeze_time("2018-08-03")
    def test_submit_result_status_ok_no_change(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.OK

        self._submit_result_and_expect(
            status_tracker, True, Status.OK, StatusChange.NONE
        )

        assert datetime(2018, 8, 3) == status_tracker.last_success

    def test_submit_result_status_warning_no_change(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.WARNING

        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NONE
        )

        assert datetime.min == status_tracker.last_success

    def test_submit_result_status_error_no_change(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.ERROR
        status_tracker._number_retry_attempts = 0

        self._submit_result_and_expect(
            status_tracker, False, Status.ERROR, StatusChange.NONE
        )

        assert datetime.min == status_tracker.last_success

    def test_submit_result_status_ok_new_warning(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.OK

        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NEW_WARNING
        )

        # Expect last success to have not changed
        assert datetime.min == status_tracker.last_success

    def test_submit_result_status_ok_new_error(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.OK
        status_tracker._number_retry_attempts = 0

        self._submit_result_and_expect(
            status_tracker, False, Status.ERROR, StatusChange.NEW_ERROR
        )

        # Expect last success to have not changed
        assert datetime.min == status_tracker.last_success

    def test_submit_result_status_warning_new_error(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.WARNING
        status_tracker._number_retry_attempts = 0

        self._submit_result_and_expect(
            status_tracker, False, Status.ERROR, StatusChange.NEW_ERROR
        )

        # Expect last success to have not changed
        assert datetime.min == status_tracker.last_success

    @freeze_time("2018-08-03")
    def test_submit_result_status_warning_resolved(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.WARNING
        status_tracker._number_retry_attempts = 0

        self._submit_result_and_expect(
            status_tracker, True, Status.OK, StatusChange.WARNING_RESOLVED
        )
        assert datetime(2018, 8, 3) == status_tracker.last_success

    @freeze_time("2018-08-03")
    def test_submit_result_status_error_resolved(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.ERROR
        status_tracker._number_retry_attempts = 0

        self._submit_result_and_expect(
            status_tracker, True, Status.OK, StatusChange.ERROR_RESOLVED
        )

        assert datetime(2018, 8, 3) == status_tracker.last_success

    def test_submit_result_retry_attempts(self):
        status_tracker = StatusTracker()

        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NEW_WARNING
        )
        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NONE
        )
        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NONE
        )
        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NONE
        )
        self._submit_result_and_expect(
            status_tracker, False, Status.WARNING, StatusChange.NONE
        )
        self._submit_result_and_expect(
            status_tracker, False, Status.ERROR, StatusChange.NEW_ERROR
        )

        # Expect last success to have not changed
        assert datetime.min == status_tracker.last_success

    def test_set_status_invalid_value_error_to_warning(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = Status.ERROR

        status_change = status_tracker._set_status(Status.WARNING)

        assert Status.WARNING == status_tracker.status
        assert StatusChange.INVALID == status_change
        assert datetime.min == status_tracker.last_success

    def test_set_status_invalid_type(self):
        status_tracker = StatusTracker()

        with pytest.raises(TypeError):
            status_tracker._set_status(-1)

        assert Status.UNKNOWN == status_tracker.status
        assert datetime.min == status_tracker.last_success

    @freeze_time("2018-08-03")
    def test_set_status_invalid_initial_state(self):
        status_tracker = StatusTracker()

        # Initial state
        status_tracker._status = -1

        status_change = status_tracker._set_status(Status.OK)

        assert Status.OK == status_tracker.status
        assert StatusChange.INVALID == status_change
        assert datetime(2018, 8, 3) == status_tracker.last_success
