from internet_monitor.run import app

import os
import yaml
import logging.config


def setup_logging() -> None:
    root_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    with open(os.path.join(root_dir, "logging.yaml"), "rt") as log_file:
        config = yaml.safe_load(log_file.read())
    logging.config.dictConfig(config)


setup_logging()

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
