# Internet Monitor

This is a small app that runs in a Docker container and monitors your internet connection. It will then save the results
to a MongoDB database.

## Development and deployment

See the [Contribution guidelines for this project](CONTRIBUTING.md) for details on how to make changes to this library.
