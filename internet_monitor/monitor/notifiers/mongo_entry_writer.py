import logging

from internet_monitor.monitor.notifiers.entry_writer_notifier import EntryWriter

from monitor_common.mongodb.status_entry import StatusEntry

logger = logging.getLogger(__name__)


class MongoEntryWriter(EntryWriter):
    def write_new_entry(self, entry: StatusEntry):
        logger.debug("Adding new entry: %s.", entry)

        entry.save()
