import logging
import json
import os

from monitor_common.mongodb.status_entry import StatusEntry

from internet_monitor.monitor.notifiers.entry_writer_notifier import EntryWriter

logger = logging.getLogger(__name__)


def prepare_json_file(file_path: str):
    logger.debug("Preparing data file at %s.", file_path)

    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    if os.path.isfile(file_path):
        logger.debug("Existing file found, do not recreate.")
        return

    with open(file_path, "w+") as new_file:
        logger.debug("No existing file found, create a new one.")
        json.dump([], new_file)


class JsonEntryWriter(EntryWriter):
    def __init__(self, output_file: str):
        self._output_file = output_file
        prepare_json_file(output_file)

    def write_new_entry(self, entry: StatusEntry):
        logger.debug("Adding new entry: %s.", entry)

        current_info = self._get_test_data()
        current_info.append(entry.to_dict())

        self._save_test_data(current_info)

    def _get_test_data(self):
        with open(self._output_file, "r") as test_data:
            data = json.load(test_data)

        return data

    def _save_test_data(self, new_test_data: list):
        with open(self._output_file, "w") as test_data:
            json.dump(new_test_data, test_data)
