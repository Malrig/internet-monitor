import logging
from datetime import datetime

from monitor_common.mongodb.status_entry import StatusEntry
from monitor_common.status import Status, StatusChange

logger = logging.getLogger(__name__)


class EntryWriter:
    def write_new_entry(self, entry: StatusEntry):
        pass


class EntryWriterNotifier:
    def __init__(self, writer: EntryWriter):
        self._writer = writer

    def notify(
        self, time: datetime, result: bool, status: Status, status_change: StatusChange
    ):
        new_entry = StatusEntry(
            time=time, result=result, status=status, status_change=status_change
        )

        self._writer.write_new_entry(new_entry)
