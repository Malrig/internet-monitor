from internet_monitor.app import app

import logging
import os
import mongoengine
from apscheduler.schedulers.background import BackgroundScheduler

from internet_monitor.monitor.monitor import Monitor
from internet_monitor.monitor.connection_tester import ConnectionTester
from internet_monitor.monitor.status_tracker import StatusTracker
from internet_monitor.monitor.notifiers.entry_writer_notifier import EntryWriterNotifier
from internet_monitor.monitor.notifiers.mongo_entry_writer import MongoEntryWriter


logger = logging.getLogger(__name__)


def _get_optional_config(variable_name, default=None):
    try:
        return os.environ[variable_name]
    except KeyError:
        return default


def run_monitor():
    connection_string = _get_optional_config("MONGODB_CONNECTION_STRING")
    test_interval = _get_optional_config("TEST_INTERVAL", 300)
    retry_interval = _get_optional_config("RETRY_INTERVAL", 15)
    error_interval = _get_optional_config("ERROR_INTERVAL", 60)

    # mongodb://root:rootpassword@host.docker.internal

    scheduler = BackgroundScheduler(standalone=False)
    connection_tester = ConnectionTester()
    status_tracker = StatusTracker()
    mongoengine.connect(db="monitor_test", alias="monitor", host=connection_string)
    mongo_writer = MongoEntryWriter()
    notifier = EntryWriterNotifier(mongo_writer)
    monitor_config = {
        "test_interval": test_interval,
        "retry_interval": retry_interval,
        "error_interval": error_interval,
    }
    monitor = Monitor(
        scheduler, connection_tester, status_tracker, notifier, monitor_config
    )

    try:
        logger.info("Run the first initial test.")
        monitor.run_test()
        logger.info("Start the scheduler proper.")
        scheduler.start()

    except (KeyboardInterrupt, SystemExit):
        logger.info("Shutting down the scheduler.")
        scheduler.shutdown(wait=False)


run_monitor()
