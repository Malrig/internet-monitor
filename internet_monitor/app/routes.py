import json
from datetime import datetime

from internet_monitor.app import app
from monitor_common.mongodb.status_entry import StatusEntry


@app.route("/")
@app.route("/index")
def get():
    return "hello world!"


# Get all statuses in a particular time range
@app.route("/entries")
def get_all_entries():
    entries = StatusEntry.objects().exclude("id")

    return json.dumps([entry.to_dict() for entry in entries])


# Get all statuses in a particular time range
@app.route("/entries/<string:range_start>/<string:range_end>")
def get_entries_in_range(range_start, range_end):
    datetime_start = datetime.strptime(range_start, "%Y%m%dT%H%M%SZ")
    datetime_end = datetime.strptime(range_end, "%Y%m%dT%H%M%SZ")
    entries = StatusEntry.objects(
        time__gte=datetime_start, time__lte=datetime_end
    ).exclude("id")

    return json.dumps([entry.to_dict() for entry in entries])


# Get overall status for an hour
# Can break up into 15 minute chunks

# Get overall status for a day
# Can split up into hour chunks
