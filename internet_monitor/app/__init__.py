from flask import Flask

app = Flask(__name__)

from internet_monitor.app import routes
